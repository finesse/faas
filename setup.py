from setuptools import setup, find_packages

with open("README.md") as readme_file:
    README = readme_file.read()

REQUIREMENTS = [
    "pykat >= 1.1.380", # NOTE: actually needs a new release to support plot grid kwarg / parsing exceptions.
    "flask >= 1.1",
    "flask_cors",
    "webargs >= 5.5",
    "redis >= 3.0",
    "rq >= 1.1",
    "rq-dashboard",
    "setproctitle", # for nicer rq worker process names
]

setup(
    name="faas",
    description="Finesse as a Service",
    long_description=README,
    version="0.2.0",
    author="Sean Leavey",
    author_email="sean.leavey@ligo.org",
    url="https://git.ligo.org/finesse/faas/",
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    license="GPLv3",
    zip_safe=False,
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ]
)
