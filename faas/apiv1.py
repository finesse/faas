"""Finesse REST API v1"""

import os
from math import ceil
from collections import defaultdict
from pprint import pformat
import numpy as np
from flask import Blueprint, Response, url_for, request, jsonify, render_template, make_response, send_file
from webargs.flaskparser import use_kwargs
from marshmallow import Schema, fields
from .jobs import KatJob, enqueue_kat_job, get_kat_jobs

KAT_DATA_TYPES = ["kat", "normkat", "inputkat", "stdout", "out"]
KAT_FAIL_DATA_TYPES = ["inputkat"]
PLOT_FORMATS = ["pdf", "png", "svg"]
JOBS_PER_PAGE = 25

app = Blueprint("apiv1", __name__)


class PlotSchema(Schema):
    """Plot query string schema."""
    legend = fields.Boolean()
    loc = fields.Str()
    title = fields.Str()
    xlabel = fields.Str()
    x2label = fields.Str()
    ylabel = fields.Str()
    y2label = fields.Str()
    grid = fields.Boolean()

plot_schema = PlotSchema()


class JobNotFinishedResponse(Response):
    def __init__(self, job):
        payload = jsonify(message=f"Results for job {str(job.id)} not yet available.")
        super().__init__(payload, 503)


class JobFailedResponse(Response):
    def __init__(self, job):
        payload = jsonify(
            message=f"Job {str(job.id)} failed.",
            kat_message=job.error,
        )
        super().__init__(payload, 500)

@app.route("/run", methods=["POST"])
def run():
    """Run Finesse script."""
    try:
        job = KatJob.from_script(request.form["script"])
    except LookupError:
        job = enqueue_kat_job(request.form["script"], request.form.get("title"), request.form.get("description"))
        msg = "Job queued."
        code = 202
    else:
        msg = "Job already exists in cache."
        code = 200
    return make_response(jsonify(message=msg, job=kat_run_job_info(job)), code)

@app.route("/jobs/search/<query>/<int:page>")
@app.route("/jobs/search/<query>")
@app.route("/jobs/<int:page>")
@app.route("/jobs")
def jobs(page=1, query=None):
    """List jobs."""
    def date_key(job):
        if job.is_finished:
            return job.finish_date
        return None
    
    try:
        page = int(page)
        if page < 1:
            raise
    except:
        page = 1
    
    # Sort descending by date.
    jobs = sorted(get_kat_jobs(title=query, description=query, script=query), key=date_key, reverse=True)

    pages = ceil(len(jobs) / JOBS_PER_PAGE)
    start = (page - 1) * JOBS_PER_PAGE

    job_statuses = [kat_run_job_info(job) for job in jobs[start:start+JOBS_PER_PAGE]]

    return make_response(jsonify(page=page, pages=pages, per_page=JOBS_PER_PAGE, jobs=job_statuses), 200)

@app.route("/job/<job_id:job>")
def job(job):
    """List job."""
    return make_response(jsonify(kat_run_job_info(job)), 200)

@app.route("/job/<job_id:job>/data/<any(" + ", ".join(KAT_DATA_TYPES) + "):data_type>")
def job_data(job, data_type=None):
    if not job.is_finished:
        return JobNotFinishedResponse(job)

    if data_type == "kat":
        response = make_response(job.script)
        response.headers["Content-Type"] = "text/plain; charset=utf-8"
        return response
    elif data_type == "normkat":
        response = make_response(job.normscript)
        response.headers["Content-Type"] = "text/plain; charset=utf-8"
        return response
    elif data_type == "inputkat":
        response = make_response(job.original_script)
        response.headers["Content-Type"] = "text/plain; charset=utf-8"
        return response
    elif data_type == "stdout":
        response = make_response(job.results.stdout)
        response.headers["Content-Type"] = "text/plain; charset=utf-8"
        return response
    elif data_type == "out":
        data = np.hstack((np.expand_dims(job.results.x, 1), job.results.y))
        ylabels = ", ".join(job.results.ylabels)
        datastr = f"# xlabel: {job.results.xlabel}\n# ylabels: {ylabels}\n" + pformat(data.tolist())
        response = make_response(datastr)
        response.headers["Content-Type"] = "text/plain; charset=utf-8"
        return response

@app.route("/job/<job_id:job>/plot")
@app.route("/job/<job_id:job>/plot/plot.<any(" + ", ".join(PLOT_FORMATS) + "):plot_format>")
@use_kwargs(plot_schema)
def job_plot(job, plot_format=None, grid=True, **kwargs):
    if not job.is_finished:
        return JobNotFinishedResponse(job)
    if job.results_have_error:
        return JobFailedResponse(job)

    if plot_format is None:
        plot_format = "svg"

    filename = str(job.plot(plot_format, grid=grid, **kwargs))
    return send_file(filename)

def kat_run_job_info(job):
    """Get job info."""
    data = dict(
        uuid=job.job_id,
        status=job.status,
        title=job.title,
        description=job.description,
    )

    if job.is_finished:
        extra = dict(
            run_time=job.run_time.total_seconds(),
            start_date=job.start_date.isoformat(),
            finish_date=job.finish_date.isoformat(),
        )

        if job.results_have_error:
            extra["error_msg"] = job.error_msg
        else:
            extra["finesse_version"] = job.finesse_version
            extra["finesse_version_full"] = job.finesse_version_full
        
        # Routes are present in all job status circumstances.
        extra["routes"] = kat_run_resource_info(job)

        data = {**data, **extra}

    return data

def kat_run_resource_info(job):
    """Get dictionary of routes available for the specified (finished) job."""
    if not job.is_finished:
        # No resources available.
        return dict()

    outputs = defaultdict(dict)
    # Textual results.
    for data in KAT_DATA_TYPES:
        if job.results_have_error and data not in KAT_FAIL_DATA_TYPES:
            continue
        outputs["data"][data] = url_for("apiv1.job_data", job=job, data_type=data, _external=True)
    # Plotting.
    if not job.results_have_error:
        outputs["plots"] = defaultdict(dict)
        for ext in PLOT_FORMATS:
            outputs["plots"]["formats"][ext] = url_for("apiv1.job_plot", job=job, plot_format=ext, _external=True)
        outputs["plots"]["params"] = {param: field.__class__.__name__ for param, field in plot_schema.fields.items()}
    
    job = url_for("apiv1.job", job=job, _external=True)

    return dict(job=job, outputs=outputs)