import os
from configparser import ConfigParser

CONFIG = ConfigParser()
CONFIG.read(os.getenv("FAAS_CONFIG"))

# Change directory to working directory. This seems necessary to prevent matplotlib
# complaining about not finding the current working directory when faas is running
# as a service.
os.chdir(CONFIG["package"]["working_directory"])

# Use headless backend.
# This import has to be before anything else that might import matplotlib (e.g. pykat).
import matplotlib
matplotlib.use('Agg')
