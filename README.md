# Finesse as a Service
Finesse web service

## Requirements
```
conda install redis
cd /path/to/faas
pip install -e .
```

## Usage
Start redis:
```
cd /path/to/faas
redis-server
```

Start rq worker(s):
```
cd /path/to/faas
rq worker
```

Start flask web server:
```
cd /path/to/faas/faas
env FLASK_APP=api.py flask run
```