from pathlib import Path
import datetime
import pickle
from hashlib import md5
from zlib import adler32
from uuid import uuid4
import re
import json
from pykat import finesse
from pykat.exceptions import FinesseRunError, BasePyKatException
from redis import Redis
from rq import Queue, get_current_job as get_current_rq_job
from rq.job import Job as RQJob
from rq.registry import StartedJobRegistry
from . import CONFIG

RESULTS_DIR = Path(CONFIG["results"]["directory"]).resolve()

# Set up the job queue.
RCONF = CONFIG["redis"]
JOB_QUEUE_DB = Redis(host=RCONF["host"], port=RCONF["port"], db=RCONF["job_queue_db"])
JOB_QUEUE = Queue(connection=JOB_QUEUE_DB)
STARTED_JOBS = StartedJobRegistry(connection=JOB_QUEUE_DB)

# Set up the cache.
CACHE = Redis(host=RCONF["host"], port=RCONF["port"], db=RCONF["job_cache_db"], decode_responses=True)


class KatJob:
    """Kat job."""
    def __init__(self, job_id):
        self.job_id = str(job_id)
        self._results = None
        self._error = None

    @classmethod
    def from_script(cls, script):
        # Check if hash is in cache.
        script_hash = calculate_kat_script_hash(script)
        job_id = get_cached_job_id(script_hash)
        if job_id is not None:
            return cls(job_id)
        raise LookupError(f"Kat script with hash {script_hash} not found in cache.")

    @classmethod
    def from_uuid(cls, uuid):
        return cls(str(uuid))

    @classmethod
    def from_rq_job(cls, rq_job):
        return cls(str(rq_job.id))

    @property
    def rq_job(self):
        return RQJob.fetch(self.job_id, connection=JOB_QUEUE_DB)

    @property
    def rq_status(self):
        # Possible: queued, started, deferred, finished, failed.
        return self.rq_job.get_status()

    @property
    def status(self):
        if self.is_finished and self.results_have_error:
            return "failed"
        return self.rq_status

    @property
    def results(self):
        if self._results is None:
            with open(self.results_path, "rb") as fobj:
                self._results = pickle.load(fobj)
        return self._results

    @property
    def job_dir(self):
        return get_job_dir(self.job_id)

    @property
    def results_path(self):
        return self.job_dir / "results"

    @property
    def exists(self):
        return (
            self.is_finished
            or self.job_id in JOB_QUEUE.get_job_ids()
            or self.job_id in STARTED_JOBS.get_job_ids()
        )

    @property
    def is_finished(self):
        return self.results_path.is_file()

    @property
    def start_date(self):
        return self.results.start_date

    @property
    def finish_date(self):
        return self.results.finish_date

    @property
    def results_have_error(self):
        return self.results.error_msg is not None
    
    @property
    def error_msg(self):
        return self.results.error_msg

    @property
    def title(self):
        if self.is_finished:
            return self.results.title
        return self.rq_job.kwargs.get("title")

    @property
    def description(self):
        if self.is_finished:
            return self.results.description
        return self.rq_job.kwargs.get("description")

    @property
    def script(self):
        return self.results.output_script

    @property
    def normscript(self):
        return normalise_kat_script(self.script)

    @property
    def original_script(self):
        return self.results.input_script

    @property
    def finesse_version(self):
        """The Finesse version the results were computed with."""
        return self.results.finesse_version
    
    @property
    def finesse_version_full(self):
        """The full Finesse version the results were computed with."""
        return self.results.finesse_version_full
        
    @property
    def run_time(self):
        """Run time reported by Finesse."""
        return self.results.run_time

    def ingest_run(self, run):
        self.job_dir.mkdir(exist_ok=True)

        with open(self.results_path, "wb") as fobj:
            pickle.dump(run, fobj)

    def plot(self, plot_format, **kwargs):
        """Generate and return the path to a plot of the results."""
        filename = self._plot_path(plot_format, **kwargs)
        if not filename.is_file():
            self.results.plot(filename=filename, show=False, **kwargs)
        return filename

    def _plot_path(self, plot_format, **plot_kwargs):
        # Compute a deterministic hash of the plot options.
        checks = (self.job_id, plot_format, plot_kwargs)
        checksum = adler32(bytes(json.dumps(checks, sort_keys=True).encode("utf-8")))
        return self.job_dir / f"{checksum}.{plot_format}"


class KatRun:
    def __init__(self, script, pykat_run=None):
        # Note: we require the user-defined script here even though the Pykat KatRun object contains it
        # because Pykat adds extra kat script lines, e.g. gnuterm, during its run, yet we need the original
        # script to deduplicate user-submitted scripts.
        self.input_script = script
        self.pykat_run = pykat_run
        
        self.error_msg = None
        self.start_date = None
        self.finish_date = None
        self.finesse_version_full = None

        self._title = None
        self._description = None

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        if not title:
            # Avoid setting empty strings.
            return
        title = str(title)
        title = title[:int(CONFIG["jobs"]["max_title_length"])]
        self._title = title

    @property
    def description(self):
        return self._description
    
    @description.setter
    def description(self, description):
        if not description:
            # Avoid setting empty strings.
            return
        description = str(description)
        description = description[:int(CONFIG["jobs"]["max_description_length"])]
        self._description = description
    
    @property
    def output_script(self):
        return self.pykat_run.katScript
    
    @property
    def finesse_version(self):
        return self.pykat_run.katVersion
    
    @property
    def finesse_run_time(self):
        return self.pykat_run.runtime
    
    @property
    def run_time(self):
        return self.finish_date - self.start_date
    
    def plot(self, *args, **kwargs):
        return self.pykat_run.plot(*args, **kwargs)

    @property
    def stdout(self):
        return sanitise_terminal_output(self.pykat_run.stdout)

    @property
    def x(self):
        return self.pykat_run.x

    @property
    def y(self):
        return self.pykat_run.y
    
    @property
    def xlabel(self):
        return self.pykat_run.xlabel

    @property
    def ylabels(self):
        return self.pykat_run.ylabels


def get_job_dir(job_id):
    return RESULTS_DIR / str(job_id)

def unique_job_id():
    def _new_id():
        return str(uuid4())
    sample = _new_id()
    while get_job_dir(sample).is_dir():
        sample = _new_id()
    return sample

def enqueue_kat_job(script, title=None, description=None):
    """Enqueue kat job and return a new KatJob object."""
    # Normalise the submitted strings.
    if title is not None:
        title = "\n".join(title.strip().splitlines())
    if description is not None:
        description = "\n".join(description.strip().splitlines())

    # Start the job.
    rq_job = JOB_QUEUE.enqueue(
        run_kat,
        args=(script,),
        kwargs=dict(title=title, description=description),
        id=unique_job_id(),
        timeout="1h",
        # Store the result in redis indefinitely.
        result_ttl=-1
    )
    return KatJob.from_rq_job(rq_job)

def build_job_cache():
    # Delete current cache.
    CACHE.flushdb()
    # Create results directory if necessary.
    RESULTS_DIR.mkdir(exist_ok=True)
    # Cache jobs on file system.
    for result_dir in [rdir for rdir in RESULTS_DIR.iterdir() if rdir.is_dir()]:
        basename = result_dir.name
        cache_job(KatJob(basename))

def cache_job(job):
    script_hash = calculate_kat_script_hash(job.original_script)
    CACHE.set(script_hash, job.job_id)

def get_cached_job_id(script_hash):
    return CACHE.get(script_hash)

def run_kat(script, title=None, description=None):
    kat = finesse.kat()

    rq_job = get_current_rq_job()
    kat_job = KatJob(rq_job.id)

    run = KatRun(script)
    run.title = title
    run.description = description

    run.start_date = datetime.datetime.now(datetime.timezone.utc)

    try:
        kat.parse(script)
    except BasePyKatException as run_error:
        run.error_msg = str(run_error)
    else:
        try:
            run.pykat_run = kat.run(rethrowExceptions=True)
        except FinesseRunError as run_error:
            run.error_msg = sanitise_terminal_output(run_error)
        except BasePyKatException as run_error:
            run.error_msg = str(run_error)
    
    run.finish_date = datetime.datetime.now(datetime.timezone.utc)
    run.finesse_version_full = kat.finesse_version()
    
    kat_job.ingest_run(run)
    cache_job(kat_job)
    
    return True

def calculate_kat_script_hash(script):
    hashstr = md5()

    # Hash the script and the current Finesse version.
    hashstr.update(normalise_kat_script(script).encode("utf-8"))
    hashstr.update(get_kat_version().encode("utf-8"))

    return hashstr.hexdigest()

def normalise_kat_script(script):
    # Remove leading/trailing whitespace, comments and empty lines.
    lines = []
    for line in script.splitlines():
        # Get rid of trailing comments and whitespace.
        line = line.split("#")[0].split("%")[0].strip()
        # Normalise whitespace.
        line = " ".join(line.split())
        if not line:
            # Skip empty line.
            continue
        lines.append(line)
    script = "\n".join(lines)
    return script

def get_kat_jobs(title=None, description=None, script=None, queued=True, running=True, completed=True):

    def _yield_jobs():
        found_job_ids = []

        if queued:
            for job_id in JOB_QUEUE.get_job_ids():
                found_job_ids.append(job_id)
                yield KatJob(job_id)

        if running:
            for job_id in STARTED_JOBS.get_job_ids():
                if job_id not in found_job_ids:
                    found_job_ids.append(job_id)
                    yield KatJob(job_id)
                    
        if completed:
            for key in CACHE.keys():
                job_id = str(CACHE.get(key))
                if job_id not in found_job_ids:
                    found_job_ids.append(job_id)
                    yield KatJob(job_id)

    for job in _yield_jobs():
        if (
            (title is not None and title.lower() not in job.title.lower()) and
            (description is not None and description.lower() not in job.description.lower()) and
            (script is not None and script.lower() not in job.original_script.lower())
        ):
            continue

        yield job

def get_kat_version():
    kat = finesse.kat()
    return kat.finesse_version()

def sanitise_terminal_output(error_msg):
    """Sanitise a kat error message, removing ANSI escape sequences if necessary.
    
    https://stackoverflow.com/a/38662876/2251982
    """
    error_msg = str(error_msg)
    ansi_escape = re.compile(r'(?:\x1B[@-_]|[\x80-\x9F])[0-?]*[ -/]*[@-~]')
    return ansi_escape.sub('', error_msg)