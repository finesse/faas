"""Routes"""

from werkzeug.routing import UUIDConverter
from werkzeug.exceptions import NotFound
from rq.job import NoSuchJobError
from .jobs import KatJob


class JobIdConverter(UUIDConverter):
    """Resolve generic UUID to a job."""
    def to_python(self, value):
        uuid = super().to_python(value)
        job = KatJob.from_uuid(uuid)
        if not job.exists:
            raise NotFound(f"No such job: {uuid}")
        return job

    def to_url(self, job):
        return super().to_url(job.job_id)
