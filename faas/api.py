"""Finesse REST API"""

import rq_dashboard
from flask import Flask, request, jsonify, render_template, make_response
from flask_cors import CORS
from .routes import JobIdConverter
from .jobs import build_job_cache
from .apiv1 import app as apiv1

# Build job cache.
build_job_cache()

app = Flask(__name__)
CORS(app)
app.url_map.converters["job_id"] = JobIdConverter
app.register_blueprint(apiv1, url_prefix='/v1')

# Register dashboard.
app.config.from_object(rq_dashboard.default_settings)
app.register_blueprint(rq_dashboard.blueprint, url_prefix="/dashboard")
